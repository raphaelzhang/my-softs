# 常用的Windows软件

下面*斜体*的软件都是需要付费的软件。

## 基础软件

* 浏览器：[Chrome](https://www.google.cn/intl/zh-CN/chrome/)是很快的浏览器，而且简洁易用。另外，[Firefox](https://www.firefox.com.cn/)也是一个备选，我主要用它关闭javascript浏览网页

* 输入法：[搜狗拼音](https://pinyin.sogou.com/)

* 即时通信都是企鹅的：[QQ](https://im.qq.com/)与[微信](https://weixin.qq.com/)，QQ还支持聊天记录迁移，挺好的

* 压缩：[7-zip](http://www.7-zip.org/)与[WinRAR](http://www.winrar.com.cn/)

* 下载：[迅雷](http://dl.xunlei.com/)的广告越来越多了，也没有迅雷的各种简化版了，QQ旋风也太监了

* 影音：[PotPlayer](http://potplayer.daum.net/?lang=zh_CN)是原KMPlayer作者开发的影音播放器，不过不知道为啥支持系统上没写Windows 10

* 图像：[XnView](https://www.xnview.com/)与[IrfanView](https://www.irfanview.com/)都是很棒的看图软件

* 文档：[福昕阅读器](https://www.foxitsoftware.cn/downloads/)，即Foxit Reader，读取PDF文档非常方便，而[WinDjView](https://windjview.sourceforge.io/)读取DjVu文档非常方便

* 视频：[ffmpeg](http://www.ffmpeg.org/download.html)是非常优秀的视频处理工具，支持格式多，跨平台，而且免费开源，Windows版戳[这里](https://ffmpeg.zeranoe.com/builds/)。当然它的缺点是要用命令行，如：

    * 3gp转为720p的mp4：`ffmpeg -i inputfile.3gp -s 1280x720 -c:a libvo_aacenc -c:v libx264 -f mp4 outputfile.mp4`，配上H5的video标签就可以直接在浏览器里看了（HPS）

    * avi转mp4：`ffmpeg -i my.avi -f mp4　-acodec libvo_aacenc -vcodec libx264 -s 1440x960 -b 512k -ab 320k my.mp4`

    * 将`001.ts`到`006.ts`合并成一个mp4：`ffmpeg -f concat -i files.txt -c copy my.mp4`，其中`files.txt`的内容见后

    * 将mp4分解成ts（HLS）：`ffmpeg -i inputfile.avi -f mp4　-acodec libvo_aacenc -vcodec libx264 -s 720x480 -b 512k -ab 320k outputfile.mp4`

    * 或者`ffmpeg -i inputfile.avi -f segment -map 0 -s 720x480  -c:v libx264 -c:a libvo_aacenc -segment_list_flags +live -segment_list output.m3u8 -segment_time 10 -segment_format mpegts output%05d.ts`

```
file './001.ts'
file './002.ts'
...
file './006.ts'
```

* 虚拟机：[VirtualBox](https://www.virtualbox.org/wiki/Downloads)是免费而方便的虚拟机软件，调试跨平台的功能很方便，不过如果需要安装64位虚拟机要注意打开BIOS里的CPU虚拟化设置

* 网盘：[百度网盘](https://pan.baidu.com/download)是国内唯一能正经用的网盘了吧，很郁闷的是现在必须使用客户端才能下载了

* 个人：各银行的电子银行客户端、支付宝、淘宝旺旺之类的，如[招行专业版](http://www.cmbchina.com/pbankweb/download.htm)

## 编辑器

* _[EverEdit](http://cn.everedit.net/)_ 是一个很棒的文本编辑器，它功能简洁，体积小，我用的几个语言都支持，包括Python、Go、Javascript、HTML、CSS、Shell、C、CPP、PHP、BAT、C#、Haskell、Makefile、INI、Pascal、SQL，主要看中的是语法高亮与代码折叠，支持UTF8/ANSI等各种文件格式，写程序足矣，而且复制成RTF功能方便写文档，完美替代[Highlight](https://chocolatey.org/packages/highlight/)，而且支持二进制文件与超大文件。主要问题是语法不够新，例如es6的语法，还有当然就是价格的问题了，毕竟免费的优秀编辑器也不少

* [Visual Studio Code](https://code.visualstudio.com/)如果不考虑十六进制的问题，微软的这个编辑器也是非常不错的，跨平台、基础的Git支持、Markdown等多语言支持等都很不错，调试nodejs程序也很方便。缺点是集成的git用起来比较鸡肋，不够强大。当然，最主要的问题还是太耗资源，轻轻松松消耗500M+

* [VNote](https://github.com/tamlok/vnote)也是一个中国人做的软件，比较好地支持Markdown编辑，不过感觉[typora](https://typora.io/)更强大，界面也更简洁，虽然Windows在此时还是β版

## Linux相关

* Windows上能通过WSL用Ubuntu等发行版了，虽然只是console的，也足够了，毕竟有vim就行

* 现在Windows上已经有ssh、scp、sftp、curl和tar了，顺便就可以不用[PuTTY](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html)与[WinSCP](http://winscp.net/)了。当然，实际上，有了WSL后也可以不用了

* 在Windows上用git可以用[Git for Windows](https://gitforwindows.org/)，[SourceTree](https://www.sourcetreeapp.com/)也不错，同时支持Git和Hg，但是注册比较麻烦，而且自带的python版hg比较buggy，用起来感觉还是老牌的[TortoiseHg](https://tortoisehg.bitbucket.io/download/index.html)比较靠谱。要是自建仓库后台，[gitea](https://gitea.io/zh-cn/)比gitlab显然更方便。

* 架设SSH服务器可以使用[MobaSSH Server for Windows](https://mobassh.mobatek.net/download.html)，非常方便。不过Windows也在开发[sshd](https://github.com/PowerShell/openssh-portable)...

## 其它开发软件

* 文件差异比较使用*Beyond Compare*或者*Araxis Merge*都是很好的，免费的没有特别好的，也许[WinMerge 2](https://github.com/WinMerge/winmerge-v2/releases)可以

* [Visual Studio社区版](https://visualstudio.microsoft.com/zh-hans/vs/community/)免费又强大，其它的IDE可以都参考[JetBrains](http://www.jetbrains.com/)家的，不过这些软件都非常消耗资源

* [nwjs](https://github.com/nwjs/nw.js)，即node-webkit，它现在用的是Chromium，内建nodejs(原来是io.js，但是分分合合的又和nodejs合并了)支持，你可以使用它来开发出基于HTML与nodejs的、本地化的、非常炫的界面(HTML+CSS大法无所不能)，但是体积、资源占用都大了些，而且代码保密不太容易。当然，现在更流行的是GitHub出品的[electron](https://electronjs.org/)了，nwjs和electron的主要问题就是太大了，一个包100兆左右甚至更多

* [ActivePython](http://www.activestate.com/activepython/downloads)是Windows上非常好的Python发行包，当然，我们还需要安装一些其他的Python包

	* [requests](http://docs.python-requests.org/en/master/)做HTTP处理挺方便的
	
    * [pillow](https://pillow.readthedocs.io/)是代替原来的[PIL](http://www.pythonware.com/products/pil/)的，用来处理图像非常方便，读取，改写，转换，保存，创建，底层数据访问等都支持都不错
	
	* [lxml](https://lxml.de/)用来处理HTML很便利，兼容性好，效率高(比起BeautifulSoup来说)。但是输入的最好是unicode，除非是XML包装的HTML，`dom = lxml.html.fromstring(html)`，解析就是这么简单
	
	* [numpy](http://www.numpy.org/)与[scipy](https://www.scipy.org/)也许我们实际用得不多，但是在数学运算的时候确实需要，而且很多包都依赖它们，例如下面的matplotlib
	
	* [matplotlib](https://matplotlib.org/)用来画曲线图，统计图，曲面图等都非常好(原作者已经去世)
	
	* [chardet](https://pypi.python.org/pypi/chardet)可以用来推测网页的编码

    * 有不少Python库只有源代码，而没有二进制安装文件，如果要在Windows上使用它们就需要安装VC编译，如果你不希望使用VC，可以在[这里](https://www.lfd.uci.edu/~gohlke/pythonlibs/)看看是否有现成的二进制安装可用
	
	* 如果一定要在Windows下编译，安装一下Visual Studio，然后参考[StackOverflow上的这篇文章](https://stackoverflow.com/questions/2817869/error-unable-to-find-vcvarsall-bat)

* Go可以在[官方](https://golang.org/dl/)或者[Golang中国](https://www.golangtc.com/download)下载

* [phantomjs](http://phantomjs.org/)可以模拟浏览器访问网站，进行网站自动化测试与测量应该不错，它与[HarViewer](https://github.com/janodvarko/harviewer)配合可以以Chrome/Firefox常见的方式查看与监控流量。但由于有了headless chrome，phantomjs已经停更了，要实现类似功能需要首先装一个[nodejs](https://nodejs.org/en/download/)，然后`npm i puppeteer`写[puppeteer](https://pptr.dev/)的脚本来做。
* 
	* 用chrome headless将百度首页保存为一个PDF：`chrome --headless --disable-gpu --enable-logging --window-size="1280,1024" --print-to-pdf="d:\baidu.pdf" https://www.baidu.com`

	* 用chrome headless将百度首页保存为一个PNG：`chrome --headless --disable-gpu --enable-logging --window-size="1280,1024" --screenshot="d:\baidu.png" https://www.baidu.com`

	* 这里有一个puppeteer写的正文提取的例子：[pptr-extractor](https://bitbucket.org/raphaelzhang/puppeteer-extractor/src)

* 查看HTTP流量可以使用[Fiddler](https://www.telerik.com/download/fiddler)，它还可以改写Web流量，做本地调试非常方便的

## 其它软件

* 现在Windows 10已经支持直接挂在iso文件当做虚拟光盘了，不过有时候（比如有个目录小文件特别多，而且我们不会修改时）我们需要创建iso。可以在WSL或者VirtualBox里运行`genisoimage -JRV my-iso-title -o ~/backup.iso ./files/`，而Windows 10也支持直接把iso刻录成光盘了
* 视频转码我本来自己写了一个软件，但是市面上还是有一些开源或者免费，而且好用的转码软件的，维基参见[List of video transcoding software](http://en.wikipedia.org/wiki/List_of_video_transcoding_software)，正在使用[MediaCoder](http://www.mediacoderhq.com/)
* [WPS Office](http://www.wps.cn/)比Office来说虽然稍弱，但是没有盗版的问题了（有广告）。当然，你也可以用*Microsoft Office*。Visio的话暂时没办法，但是也许[ProcessOn](https://www.processon.com/)可以试试，_[亿图](http://www.edrawsoft.cn/)_ 也是一个选择，它支持的图示确实很多，甚至包括物理实验图示
* [Graphviz](http://www.graphviz.org/)可以用来画流程图，数据结构图，UML图、拓扑图等，对程序自动生成的图来说非常强大，写文档也很方便。由于是文本格式的，因此也方便进行版本控制，但是美化效果有限，因此难以做商业图，擅长做技术图，美化可以参见[这篇文章](http://icodeit.org/2015/11/using-graphviz-drawing/)
* [Sysinternals的套件](https://docs.microsoft.com/zh-cn/sysinternals/)中有不少有用的，不妨都下载下来试试
* [llvm](http://llvm.org/)做编译器后端看来是没疑问的了，不过在很多情况下你可能只需要[SWIG](http://www.swig.org/)+[CLang](http://clang.llvm.org/)就够了，要不然就是gcc/vc之类的。当然，如果你时间够的话，再用Eclipse或者[MPS](https://www.jetbrains.com/mps/)做一个IDE那就更酷了
* [TeXmacs](http://www.texmacs.org/)是非常棒的可见即所得的数学文档编辑软件，可以很方便地编辑出数学公式来，配合Graphviz更佳，如果不喜欢手写TeX与LaTeX可以尝试下，而且还可以使用Lisp之类的语言内嵌进去。当然，如果要写强大的LaTeX，则可以使用[Tex Live](https://tug.org/texlive/)。如果是要在HTML或者博客里展示数学公式的话[MathJax](http://www.mathjax.org/)则是最好的选择，当然，上面说的Typora也不错
* 查看目录大小在Windows下可以用[SpaceSniffer](http://www.uderzo.it/main_products/space_sniffer/)，在Linux下可以用ncdu
* 我以前__从来__不装杀毒软件，但是最近确实不小心中招了，发现[火绒](<https://www.huorong.cn/>)还是不错的